import React from 'react';
import './Custom.css';

function App() {
    const styles = {
        outerDiv: {
            padding: "70px 0",
            backgroundColor: "tomato",
        },
    };

    const someText = 'some text';
    const someDiv = <div style={{ backgroundColor: "white", margin: '50px', padding: '10px' }}>some div</div>

    return (
        <div style={styles.outerDiv}>
            <div className='innerdiv'>{someText}</div>
            {someDiv}
        </div>
    );
}

export default App;
