import React from 'react';
import './App.css';
import Card from './Card';
import Faker from "faker";

function App() {
    const buttonsMarkup = (
        <div>
            <button className="button button2">Yes</button>
            <button className="button button3">No</button>
        </div>
    );
    return (
        <div className="App">
            <Card
                image={Faker.image.avatar()}
                name={`${Faker.name.firstName()} ${Faker.name.lastName()}`}
                jobTitle={Faker.name.jobTitle()}>
                {buttonsMarkup}
            </Card >
            <Card
                image={Faker.image.avatar()}
                name={`${Faker.name.firstName()} ${Faker.name.lastName()}`}
                jobTitle={Faker.name.jobTitle()}>
                {buttonsMarkup}
            </Card >
            <Card
                image={Faker.image.avatar()}
                name={`${Faker.name.firstName()} ${Faker.name.lastName()}`}
                jobTitle={Faker.name.jobTitle()}>
                {buttonsMarkup}
            </Card >

        </div>
    );
}

export default App;