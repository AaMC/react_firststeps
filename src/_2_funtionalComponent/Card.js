import React from "react";
import './App.css'

const Card = (props) => {
    return (
        <div className="App">
            <div className="card">
                <img src={props.image || 'https://www.w3schools.com/howto/img_avatar.png'} alt="Avatar" style={{ width: '100%' }} />
                <div className="container">
                    <h4><b>{props.name || 'John Doe'}</b></h4>
                    <p>{props.jobTitle || 'Architect & Engineer'}</p>
                    <p> {props.children}</p>
                </div>
            </div>
        </div>
    );
}

export default Card;